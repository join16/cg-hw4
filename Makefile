

SRC = $(wildcard src/lib/*.cpp)
MAIN = src/main.cpp
EXECUTABLE = output

SRC += $(wildcard src/components/*.cpp)
SRC += $(wildcard src/vendor/*.cpp)

OBJS = $(patsubst %.cpp,%.o, $(SRC))

CXX = g++
CXXFLAGS = -std=c++11

OPENGL_LINKS = -framework OpenGL -framework GLUT

.SUFFIXES: .cpp .o
.PHONY: all run clean

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(EXECUTABLE): $(MAIN) $(OBJS)
	$(CXX) $(CXXFLAGS) $(OPENGL_LINKS) -o $@ $(MAIN) $(OBJS)

# phony

all: $(EXECUTABLE)

test_ex: src/test.cpp $(OBJS)
	$(CXX) $(CXXFLAGS) $(OPENGL_LINKS) -o $@ src/test.cpp $(OBJS)

run_test: test_ex
	./test_ex

run: $(EXECUTABLE)
	./$(EXECUTABLE)

clean:
	rm -rf $(OBJS) $(EXECUTABLE)