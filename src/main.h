#ifndef CG_HW4_MAIN_H
#define CG_HW4_MAIN_H

#include "include/OpenGL.h"

// OpenGL functions
void renderScene();
void resizeScene(int w, int h);
void handleMovement(int ms);
void onKeyPressed(int key, int xx, int yy);

// particle controlling function
void applyNextParticleAction(Particle *particle);

// main function
int main(int argc, char **argv);

#endif //CG_HW4_MAIN_H
