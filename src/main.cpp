#include <iostream>
#include <cstdlib>

#include "vendor/Image.h"
#include "lib/Camera.h"
#include "lib/Sphere.h"
#include "lib/Box.h"
#include "lib/Cloth.h"
#include "main.h"

using namespace std;

// initialize camera
Camera cam(Vector(30.0f, 30.0f, 30.0f),
           Vector(0.0f, 0.0f, 0.0f),
           Vector(0.0f, 0, 1.0f));

Image *img;
Cloth *cloth;
Object *object;

Vector lightPos(0, 0, 100.0f);

Vector gravityForce(0, 0, -1.0f), windForce;

unsigned int dt = 10;

const int size = 40;

int main(int argc, char **argv) {

    srand((unsigned int) time(NULL));

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

    glutInitWindowPosition(320, 320);
    glutInitWindowSize(640, 640);
    int window = glutCreateWindow("HW4");

    img = loadBMP("images/test.bmp");

    cloth = new Cloth(20.0f, 20.0f, size, size);
    cloth->bindTexture(img);
    cloth->move(Vector(0, 0, 20));

    object = new Sphere(5.0f, Vector(15.0f, 15.0f, 5));
    //object = new Box(10, 10, 1, Vector(10.0f, 10.0f, 3.0f));

    Particle *particle;

    glEnable(GL_DEPTH_TEST);

    // lighting init
    // glEnable(GL_LIGHTING);
    // glEnable(GL_LIGHT0);
    // glEnable(GL_NORMALIZE);

    glutDisplayFunc(renderScene);
    glutReshapeFunc(resizeScene);
    glutSpecialFunc(onKeyPressed);
    glutTimerFunc(dt, handleMovement, 0);

    // starting glut main loop
    glutMainLoop();
}

void renderScene() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLfloat lightColor0[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat lightPos0[] = {lightPos.getX(), lightPos.getY(), lightPos.getZ(), 1.0f};

    // glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
    // glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);

    // glShadeModel(GL_SMOOTH);

    glLoadIdentity();

    cloth->draw();
    object->draw();

    glutSwapBuffers();
}

void resizeScene(int w, int h) {
    cam.setCamera(w, h);
}

void handleMovement(int ms) {

    // register next timer function
    glutTimerFunc(dt, handleMovement, ms + dt);

    Particle *particle;

    // adding forces
    for (unsigned int i = 0; i < size; i++) {
        for (unsigned int j = 0; j < size; j++) {
            particle = cloth->getParticle(i, j);

            // add gravity
            particle->addGravityForce(gravityForce);

            // add wind
            particle->addForce(windForce);
        }
    }

    cloth->setNextState(dt * 10 / 1000.0f);

    for (unsigned int i = 0; i < size; i++) {
        for (unsigned int j = 0; j < size; j++) {
            particle = cloth->getParticle(i, j);

            // handle collision
            object->handleCollision(particle);
        }
    }

    glutPostRedisplay();
}

void onKeyPressed(int key, int xx, int yy) {
    switch (key) {

        case ' ':
            windForce = windForce + Vector(-10.0f, -10.0f, 5.0f);
            break;

        default:
            break;
    }
}