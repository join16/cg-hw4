#ifndef CG_VECTOR_H
#define CG_VECTOR_H


class Vector {

public:
    Vector();
    Vector(const Vector &p);
    Vector(float _x, float _y, float _z);

    float getX() const;
    float getY() const;
    float getZ() const;

    void setX(float _x);
    void setY(float _y);
    void setZ(float _z);
    void set(float _x, float _y, float _z);
    void set(Vector p);

    void addX(float _x);
    void addY(float _y);
    void addZ(float _z);

    void normalize();
    void reset();

    void print();

    Vector operator+(Vector p);
    Vector operator-(Vector p);
    Vector operator*(float scale);
    Vector operator*(unsigned int scale);
    Vector operator-();

    Vector getNormalized();

    static Vector crossProduct(Vector v1, Vector v2);
    static float dotProduct(Vector v1, Vector v2);

    float getSize();

    static float getDistance(Vector v1, Vector v2);

protected:
    float x, y, z;

};


#endif //CG_VECTOR_H
