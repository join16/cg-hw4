
#include <iostream>
#include <math.h>

#include "Vector.h"

Vector::Vector() : Vector(0, 0, 0) {}

Vector::Vector(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

Vector::Vector(const Vector &p) : x(p.getX()), y(p.getY()), z(p.getZ()) {}

float Vector::getX() const {
    return x;
}
float Vector::getY() const {
    return y;
}
float Vector::getZ() const {
    return z;
}

void Vector::setX(float _x) {
    x = _x;
}
void Vector::setY(float _y) {
    y = _y;
}
void Vector::setZ(float _z) {
    z = _z;
}
void Vector::set(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
}

void Vector::set(Vector p) {
    x = p.getX();
    y = p.getY();
    z = p.getZ();
}

void Vector::addX(float _x) {
    x += _x;
}
void Vector::addY(float _y) {
    y += _y;
}
void Vector::addZ(float _z) {
    z += _z;
}

Vector Vector::operator+(Vector p) {
    return Vector(x + p.x, y + p.y, z + p.z);
}
Vector Vector::operator-(Vector p) {
    return Vector(x - p.x, y - p.y, z - p.z);
}

Vector Vector::operator*(float scale) {
    return Vector(x * scale, y * scale, z * scale);
}
Vector Vector::operator*(unsigned int scale) {
    return Vector(x * scale, y * scale, z * scale);
}

Vector Vector::operator-() {
    return Vector(*this) * -1.0f;
}

void Vector::normalize() {
    float size = getSize();

    x /= size;
    y /= size;
    z /= size;
}

void Vector::reset() {
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
}

float Vector::getSize() {
    return sqrtf((x * x) + (y * y) + (z * z));
}

float Vector::getDistance(Vector v1, Vector v2) {
    Vector diff = v1 - v2;

    return diff.getSize();
}

Vector Vector::getNormalized() {
    Vector v(*this);
    v.normalize();
    return Vector(v);
}

void Vector::print() {
    printf("(%f, %f, %f)\n", x, y, z);
}

Vector Vector::crossProduct(Vector v1, Vector v2) {
    float resultX = (v1.y * v2.z) - (v2.y * v1.z);
    float resultY = (v1.z * v2.x) - (v2.z * v1.x);
    float resultZ = (v1.x * v2.y) - (v2.x * v1.y);

    return Vector(resultX, resultY, resultZ);
}

float Vector::dotProduct(Vector v1, Vector v2) {
    float x = v1.x * v2.x;
    float y = v1.y * v2.y;
    float z = v1.z * v2.z;

    return x + y + z;
}