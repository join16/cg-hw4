#ifndef CG_HW4_OPENGL_H
#define CG_HW4_OPENGL_H

#ifdef __APPLE__

#include <GLUT/GLUT.h>

#else
#include <GL/glut.h>
#endif

#endif //CG_HW4_OPENGL_H
