#ifndef CG_HW4_PLANE_H
#define CG_HW4_PLANE_H

#include "../include/OpenGL.h"
#include "../components/Vector.h"
#include "Object.h"

class Box : public Object {

public:
    Box(float _width, float _height, float _depth, Vector _core);

    virtual bool hasCollided(Vector point);
    virtual Vector getPushForce(Vector point);
    virtual void draw();
    virtual void handleCollision(Particle *particle);

private:
    float width, height, depth;
    Vector core;

    bool isInXBoundary(float x);
    bool isInYBoundary(float y);
    bool isInZBoundary(float z);

};


#endif //CG_HW4_PLANE_H
