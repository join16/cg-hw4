#ifndef CG_HW4_SPRING_H
#define CG_HW4_SPRING_H

#include "Particle.h"

class ParticleSpring {

public:
    ParticleSpring(Particle *_p1, Particle *_p2);
    void addForceToParticles();

    Particle *p1, *p2;
    static float SPRING_CONSTANT;

private:
    float restLength;

};


#endif //CG_HW4_SPRING_H
