#ifndef CG_HW4_PARTICLE_H
#define CG_HW4_PARTICLE_H

#include <iostream>

#include "../components/Vector.h"

#define DAMPING_CONSTANT 0.001f

class Particle {

public:
    Particle();
    Particle(float _mass);

    Vector getPosition();
    Vector getPreviousPosition();
    Vector getVelocity();

    void resetPosition(Vector _position);
    void addForce(Vector force);
    void applyNextTimeStep(float dt);

    void addGravityForce(Vector gravityVector);

    void move(Vector _moveAmount);
    void translate(Vector moveAmount);

    bool isMovable();
    void fix();
    void unfix();

    void print();

private:
    Vector position;
    Vector previousPosition;
    Vector velocity;
    Vector acceleration;
    float mass;

    bool movable = true;

    Vector getForce();

};


#endif //CG_HW4_PARTICLE_H
