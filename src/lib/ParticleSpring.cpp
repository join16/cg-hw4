#include "ParticleSpring.h"

float ParticleSpring::SPRING_CONSTANT = 0.5f;

ParticleSpring::ParticleSpring(Particle *_p1, Particle *_p2) : p1(_p1), p2(_p2) {
    restLength = Vector::getDistance(p1->getPosition(), p2->getPosition());
}

void ParticleSpring::addForceToParticles() {
    Vector force;
    Vector p1p2 = p2->getPosition() - p1->getPosition();
    Vector direction = p1p2.getNormalized();

    float currentLength = p1p2.getSize();
    float dl = currentLength - restLength;

    force = direction * (dl * SPRING_CONSTANT);

    p1->move(force);
    p2->move(-force);
}