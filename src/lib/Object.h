#ifndef CG_HW4_OBJECT_H
#define CG_HW4_OBJECT_H

#include "../components/Vector.h"
#include "Particle.h"

// abstract class

class Object {

public:
    Object();

    virtual bool hasCollided(Vector point) = 0;
    virtual Vector getPushForce(Vector point) = 0;
    virtual void draw() = 0;
    virtual void handleCollision(Particle *particle) = 0;

};


#endif //CG_HW4_OBJECT_H
