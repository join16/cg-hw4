#include "Particle.h"

Particle::Particle() {}
Particle::Particle(float _mass) : mass(_mass) {}

Vector Particle::getPosition() {
    return Vector(position);
}

Vector Particle::getPreviousPosition() {
    return Vector(previousPosition);
}

Vector Particle::getVelocity() {
    return Vector(velocity);
}

Vector Particle::getForce() {
    return Vector(acceleration * mass);
}

void Particle::resetPosition(Vector _position) {
    position = _position;
    previousPosition = position;
}

void Particle::addForce(Vector force) {
    acceleration = acceleration + (force * (1 / mass));
}

void Particle::addGravityForce(Vector gravityVector) {
    // gravityVector * mass * (1 / mass) == gravityVector
    acceleration = acceleration + gravityVector;
}

void Particle::applyNextTimeStep(float dt) {
    if (!movable) {
        return;
    }

//    Vector dp = velocity * dt;
//    Vector dv = acceleration * dt;
//
//    position = position + dp;
//    velocity = velocity + dv;

    Vector dp = position - previousPosition;
    previousPosition = Vector(position);
    position = position + (dp * 0.999f) + (acceleration * (dt * dt));

    acceleration.reset();
}

void Particle::move(Vector _moveAmount) {
    if (movable) {
        position = position + _moveAmount;
    }
}

void Particle::translate(Vector moveAmount) {
    position = position + moveAmount;
    previousPosition = previousPosition + moveAmount;
}

bool Particle::isMovable() {
    return movable;
}

void Particle::fix() {
    movable = false;
}

void Particle::unfix() {
    movable = true;
}

void Particle::print() {
    printf("-----------\n");
    printf("Accelaration: ");
    acceleration.print();
    printf("Position: ");
    position.print();
    printf("Velocity: ");
    velocity.print();
    printf("-----------\n");
}