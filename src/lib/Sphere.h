#ifndef CG_HW4_SPHERE_H
#define CG_HW4_SPHERE_H

#include "../include/OpenGL.h"
#include "../components/Vector.h"
#include "Object.h"

#define SPHERE_ERROR_RANGE 0.01f

class Sphere : public Object {

public:
    Sphere(float _radius, Vector _core);

    virtual bool hasCollided(Vector point);
    virtual Vector getPushForce(Vector point);
    virtual void draw();
    virtual void handleCollision(Particle *particle);

private:
    float radius;
    Vector core;
};


#endif //CG_HW4_SPHERE_H
