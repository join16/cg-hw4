#include "Cloth.h"

Cloth::Cloth(float width, float height, unsigned int _widthCount, unsigned int _heightCount)
    : widthCount(_widthCount), heightCount(_heightCount) {
    // initial position = (0, 0, 0), (width, 0, 0), (0, height, 0), (width, height, 0)

    Particle *particle;
    float dw = width / (float) widthCount;
    float dh = height / (float) heightCount;
    float mass = 5.0f;

    for (unsigned int j = 0; j < heightCount; j++) {
        for (unsigned int i = 0; i < widthCount; i++) {
            particle = new Particle(mass);
            particle->resetPosition(Vector(dw * i, dh * j, 0));
            particles.push_back(particle);
        }
    }

    // initialize springs
    ParticleSpring *spring;
    for (unsigned int i = 0; i < widthCount; i++) {
        for (unsigned int j = 0; j < heightCount; j++) {

            // i + 1 is available
            if (i < (widthCount - 1)) {
                spring = new ParticleSpring(getParticle(i, j), getParticle(i + 1, j));
                springs.push_back(spring);
            }

            // j + 1 is available
            if (j < (heightCount - 1)) {
                spring = new ParticleSpring(getParticle(i, j), getParticle(i, j + 1));
                springs.push_back(spring);
            }

            // (i +1, j + 1) is available
            if ((i < (widthCount - 1)) && j < (heightCount - 1)) {
                spring = new ParticleSpring(getParticle(i, j), getParticle(i + 1, j + 1));
                springs.push_back(spring);

                spring = new ParticleSpring(getParticle(i + 1, j), getParticle(i, j + 1));
                springs.push_back(spring);
            }
        }
    }

    // adding bend springs
    for (unsigned int i = 0; i < widthCount; i++) {
        for (unsigned int j = 0; j < heightCount; j++) {
            // i + 1 is available
            if (i < (widthCount - 2)) {
                spring = new ParticleSpring(getParticle(i, j), getParticle(i + 2, j));
                springs.push_back(spring);
            }

            // j + 1 is available
            if (j < (heightCount - 2)) {
                spring = new ParticleSpring(getParticle(i, j), getParticle(i, j + 2));
                springs.push_back(spring);
            }

            // (i +1, j + 1) is available
            if ((i < (widthCount - 2)) && j < (heightCount - 2)) {
                spring = new ParticleSpring(getParticle(i, j), getParticle(i + 2, j + 2));
                springs.push_back(spring);

                spring = new ParticleSpring(getParticle(i + 2, j), getParticle(i, j + 2));
                springs.push_back(spring);
            }
        }
    }
}

void Cloth::bindTexture(Image *img) {
    glGenTextures(1, &textureId); //Make room for our texture
    glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
    //Map the image to the texture
    glTexImage2D(GL_TEXTURE_2D,                //Always GL_TEXTURE_2D
                 0,                            //0 for now
                 GL_RGB,                       //Format OpenGL uses for image
                 img->width, img->height,  //Width and height
                 0,                            //The border of the image
                 GL_RGB, //GL_RGB, because pixels are stored in RGB format
                 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
        //as unsigned numbers
                 img->pixels);               //The actual pixel data
}

void Cloth::move(Vector moveAmount) {
    unsigned int length = (unsigned int) particles.size();

    for (unsigned int i = 0; i < length; i ++) {
        particles[i]->translate(moveAmount);
    }
}

void Cloth::draw() {
    Vector a, b, c, d;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glColor3f(2.0f, 2.0f, 2.0f);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    for (unsigned int i = 0; i < widthCount - 1; i++) {
        for (unsigned int j = 0; j < heightCount - 1; j++) {
            a = getParticle(i, j)->getPosition();
            b = getParticle(i + 1, j)->getPosition();
            c = getParticle(i + 1, j + 1)->getPosition();
            d = getParticle(i, j + 1)->getPosition();

            glBegin(GL_TRIANGLE_FAN);
            glNormal3f(10.0f, 0.0f, 10.0f);
            glTexCoord2f(getTextureCoordinateX(i), getTextureCoordinateY(j));
            glVertex3f(a.getX(), a.getY(), a.getZ());
            glTexCoord2f(getTextureCoordinateX(i + 1), getTextureCoordinateY(j));
            glVertex3f(b.getX(), b.getY(), b.getZ());
            glTexCoord2f(getTextureCoordinateX(i + 1), getTextureCoordinateY(j + 1));
            glVertex3f(c.getX(), c.getY(), c.getZ());
            glTexCoord2f(getTextureCoordinateX(i), getTextureCoordinateY(j + 1));
            glVertex3f(d.getX(), d.getY(), d.getZ());
            glEnd();
        }
    }
}

void Cloth::drawParticles() {
    Particle *p;
    Vector v;

    glColor3f(0.5f, 0.5f, 0.5f);

    for (unsigned int i = 0; i < widthCount; i++) {
        for (unsigned int j = 0; j < heightCount; j++) {
            p = getParticle(i, j);
            v = p->getPosition();
            glPushMatrix();
            glLoadIdentity();
            glTranslatef(v.getX(), v.getY(), v.getZ());
            glutSolidSphere(0.1f, 10, 10);
            glPopMatrix();
        }
    }
}

void Cloth::drawSprings() {
    ParticleSpring *spring;
    unsigned int length = (unsigned int) springs.size();
    Vector a, b;

    glColor3f(0, 0, 1.0f);

    for (unsigned int i = 0; i < length; i++) {
        spring = springs[i];
        a = spring->p1->getPosition();
        b = spring->p2->getPosition();

        glBegin(GL_LINE_STRIP);
        glVertex3f(a.getX(), a.getY(), a.getZ());
        glVertex3f(b.getX(), b.getY(), b.getZ());
        glEnd();
    }
}

void Cloth::setNextState(float dt) {
    unsigned int length = (unsigned int) particles.size();

    applySprings();

    for (unsigned int i = 0; i < length; i++) {
        particles[i]->applyNextTimeStep(dt);
    }
}

void Cloth::applySprings() {
    unsigned int length = (unsigned int) springs.size();

    // set spring forces
    for (unsigned int time = 0; time < 10; time++) {
        for (unsigned int i = 0; i < length; i++) {
            springs[i]->addForceToParticles();
        }
    }
}

Particle *Cloth::getParticle(unsigned int i, unsigned int j) {
    return particles[getParticleIndex(i, j)];
}

unsigned int Cloth::getParticleIndex(unsigned int i, unsigned int j) {
    return (j * widthCount) + i;
}

float Cloth::getTextureCoordinateX(unsigned int i) {
    return i / (float) widthCount;
}

float Cloth::getTextureCoordinateY(unsigned int i) {
    return i / (float) heightCount;
}