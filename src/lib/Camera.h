#ifndef CG_HW4_CAMERA_H
#define CG_HW4_CAMERA_H

#include "../include/OpenGL.h"
#include "../components/Vector.h"

class Camera {

public:
    Camera();
    Camera(Vector _eye, Vector _center, Vector _up);

    void updatePerspective(float _angle, float _near, float _far);
    void setCamera(int width, int height);

    void updateVectors(Vector _eye, Vector _center, Vector _up);

private:
    Vector eye, center, up;

    float angle = 45.0f;
    float near = 1.0f;
    float far = 600.0f;
};


#endif //CG_HW4_CAMERA_H
