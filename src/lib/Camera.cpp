#include "Camera.h"

Camera::Camera() {}

Camera::Camera(Vector _eye, Vector _center, Vector _up) : Camera() {
    updateVectors(_eye, _center, _up);
}

void Camera::updatePerspective(float _angle, float _near, float _far) {
    angle = _angle;
    near = _near;
    far = _far;
}

void Camera::updateVectors(Vector _eye, Vector _center, Vector _up) {
    eye = Vector(_eye);
    center = Vector(_center);
    up = Vector(_up);
}

void Camera::setCamera(int width, int height) {

    float ratio;

    if (height == 0) {
        height = 1;
    }

    ratio = (width * 1.0f) / height;

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    gluPerspective(angle, ratio, near, far);

    gluLookAt(eye.getX(), eye.getY(), eye.getZ(),
              center.getX(), center.getY(), center.getZ(),
              up.getX(), up.getY(), up.getZ());

    // recover model view matrix
    glMatrixMode(GL_MODELVIEW);
}