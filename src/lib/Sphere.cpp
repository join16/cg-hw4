#include "Sphere.h"

Sphere::Sphere(float _radius, Vector _core)
    : Object(), radius(_radius) {
    core = Vector(_core);
}

void Sphere::draw() {
    glColor3f(0.5f, 1.0f, 0.5f);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(core.getX(), core.getY(), core.getZ());
    glutSolidSphere(radius, 50, 50);
    glPopMatrix();
}

bool Sphere::hasCollided(Vector point) {
    float distance = Vector::getDistance(core, point);

    return distance < radius * (1 + SPHERE_ERROR_RANGE);
}

Vector Sphere::getPushForce(Vector point) {
    Vector direction = point - core;
    float distance = direction.getSize();

    direction.normalize();

    return direction * (radius * (1 + SPHERE_ERROR_RANGE) - distance);
}

void Sphere::handleCollision(Particle *particle) {
    Vector position = particle->getPosition();

    if (!hasCollided(position)) {
        return;
    }

    Vector force = getPushForce(position);
    particle->move(force);
}