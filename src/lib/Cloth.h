#ifndef CG_HW4_CLOTH_H
#define CG_HW4_CLOTH_H

#include <vector>

#include "../include/OpenGL.h"
#include "../vendor/Image.h"
#include "Particle.h"
#include "ParticleSpring.h"

using namespace std;

class Cloth {

public:
    Cloth(float width, float height, unsigned int _widthCount, unsigned int _heightCount);

    Particle *getParticle(unsigned int i, unsigned int j);

    void bindTexture(Image *img);

    void move(Vector moveAmount);
    void draw();
    void applySprings();
    void setNextState(float dt); // dt is in seconds
    void drawParticles(); // for debug
    void drawSprings();

private:
    unsigned int widthCount, heightCount;
    vector<Particle*> particles;
    vector<ParticleSpring*> springs;

    GLuint textureId;

    unsigned int getParticleIndex(unsigned int i, unsigned int j);
    float getTextureCoordinateX(unsigned int i);
    float getTextureCoordinateY(unsigned int i);
};


#endif //CG_HW4_CLOTH_H
