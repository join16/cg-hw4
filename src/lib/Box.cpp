#include "Box.h"

Box::Box(float _width, float _height, float _depth, Vector _core)
    : Object(), width(_width), height(_height), depth(_depth) {
    core = Vector(_core);
}

void Box::draw() {
    glColor3f(0.5f, 1.0f, 0.5f);

    float midWidth = width / 2;
    float midHeight = height / 2;
    float midDepth = depth / 2;
    float x = core.getX();
    float y = core.getY();
    float z = core.getZ();

    // upper side
    glBegin(GL_QUADS);
    glVertex3f(x - midWidth, y - midWidth, z + midDepth);
    glVertex3f(x + midWidth, y - midWidth, z + midDepth);
    glVertex3f(x + midWidth, y + midWidth, z + midDepth);
    glVertex3f(x - midWidth, y + midWidth, z + midDepth);
    glEnd();

    // down side
    glBegin(GL_QUADS);
    glVertex3f(x - midWidth, y - midWidth, z - midDepth);
    glVertex3f(x + midWidth, y - midWidth, z - midDepth);
    glVertex3f(x + midWidth, y + midWidth, z - midDepth);
    glVertex3f(x - midWidth, y + midWidth, z - midDepth);
    glEnd();

    // front
    glBegin(GL_QUADS);
    glVertex3f(x - midWidth, y - midWidth, z - midDepth);
    glVertex3f(x - midWidth, y + midWidth, z - midDepth);
    glVertex3f(x - midWidth, y + midWidth, z + midDepth);
    glVertex3f(x - midWidth, y - midWidth, z + midDepth);
    glEnd();

    // back
    glBegin(GL_QUADS);
    glVertex3f(x + midWidth, y - midWidth, z - midDepth);
    glVertex3f(x + midWidth, y + midWidth, z - midDepth);
    glVertex3f(x + midWidth, y + midWidth, z + midDepth);
    glVertex3f(x + midWidth, y - midWidth, z + midDepth);
    glEnd();

    // left
    glBegin(GL_QUADS);
    glVertex3f(x - midWidth, y - midWidth, z - midDepth);
    glVertex3f(x + midWidth, y - midWidth, z - midDepth);
    glVertex3f(x + midWidth, y - midWidth, z + midDepth);
    glVertex3f(x - midWidth, y - midWidth, z + midDepth);
    glEnd();

    // left
    glBegin(GL_QUADS);
    glVertex3f(x - midWidth, y + midWidth, z - midDepth);
    glVertex3f(x + midWidth, y + midWidth, z - midDepth);
    glVertex3f(x + midWidth, y + midWidth, z + midDepth);
    glVertex3f(x - midWidth, y + midWidth, z + midDepth);
    glEnd();
}

bool Box::hasCollided(Vector point) {
    return isInXBoundary(point.getX()) &&
        isInYBoundary(point.getY()) &&
        isInZBoundary(point.getZ());
}

Vector Box::getPushForce(Vector point) {
    return Vector();
}

void Box::handleCollision(Particle *particle) {
    Vector position = particle->getPosition();

    if (!hasCollided(position)) {
        return;
    }

    Vector previous = particle->getPreviousPosition();
    particle->move(previous - position);
    particle->fix();
}

///// private

bool Box::isInXBoundary(float x) {
    float midWidth = width / 2;

    return (x >= (core.getX() - midWidth) && (x <= (core.getX() + midWidth)));
}

bool Box::isInYBoundary(float y) {
    float midHeight = height / 2;

    return (y >= (core.getY() - midHeight) && (y <= (core.getY() + midHeight)));
}

bool Box::isInZBoundary(float z) {
    float midDepth = depth / 2;

    return (z >= (core.getZ() - midDepth) && (z <= (core.getZ() + midDepth)));
}