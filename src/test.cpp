
#include <iostream>

#include "lib/Particle.h"
#include "lib/ParticleSpring.h"

using namespace std;

const float dt = 0.1;

int main() {
    Particle *p1, *p2;
    ParticleSpring *spring;

    p1 = new Particle(5.0f);
    p2 = new Particle(5.0f);

    p1->resetPosition(Vector(0, 0, 0));
    p2->resetPosition(Vector(20, 0, 0));

    spring = new ParticleSpring(p1, p2);

    spring->addForceToParticles();

    printf("First---\n");
    p1->applyNextTimeStep(1);
    p2->applyNextTimeStep(1);
    p1->print();
    p2->print();

    p2->move(Vector(10, 0, 0));

    while(1) {
        printf("=========== Iteration =============\n");
        spring->addForceToParticles();
        p1->applyNextTimeStep(dt);
        p2->applyNextTimeStep(dt);
        p1->getPosition().print();
        p2->getPosition().print();

        getchar();
    }

    return 0;
}